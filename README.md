# Technical notes for developers

## Switch on logging

Set debug to true in the YAML config file

Then set the debugPattern to a regular expression that matches the message headers of the log messages you want to see output to console. The regular expression should not be given with slashes around it. i.e. do:

    debugPattern : .*

not:

    debugPattern : /.*/

## Logging stuff

Logging is performed by utils.log. It takes three parameters, first the color to use for the log message, second the message header describing what is logged, and finally the data that is to be displayed.

    var utils = require('./utils')

    var foo = {bar:'baz'}

    utils.log('red', 'Before function call foo is', foo)

In order for this to work with promise chains, one can use curry from lodash/underscore like this (P is the name used for bluebird):

    var foo = {bar:'baz'}
    P(foo).tap(_.curry(utils.log)('red', 'Before function call foo is:'))

The logging will happen when the third missing parameter is supplied by the promise chain

