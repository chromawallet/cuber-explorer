module.exports = {
    batch: batch,
    doBatchWork:doBatchWork // exposed for testing purposes
}
var _ = require('lodash')

function computeBatchFrom(from, collectionLength) {
    // "from" must not be bigger than length of batch -1
    // and not be below 0
    if (from < 0) {
        from = 0
    }
    if (from >= collectionLength) {
        from = collectionLength - 1
    }
    return from
}

function computeBatchTo(to, collectionLength) {
    // if "to" is bigger than length of batch, make it length of batch
    // and not be below 0 (empty collection)
    if (to < 0) {
        to = 0
    }
    if (to > collectionLength) {
        to = collectionLength
    }
    return to
}

function batch(collection) {
    // Batches a collection into a chunk from "this.from"
    // to "this.to"
    // If these values are out of bounds of collection, change them
    // to be inside of the bounds
    // Compute offsets for first, last, next and previous batches as well,
    // or set them to false if not useful
    var res = doBatchWork(collection, this.from, this.to, this.batchSize, this.sortBy)
    this.from = res.from
    this.to = res.to
    this.collectionLength = res.collectionLength
    this.nextBatch = res.nextBatch
    this.prevBatch = res.prevBatch
    this.lastBatch = res.lastBatch
    this.firstBatch = res.firstBatch
    return res.batchedCollection
}

function doBatchWork(collection, from, to, batchSize, sortBy) {
    var collectionLength = collection.length
    from = computeBatchFrom(from, collection.length)
    to = computeBatchTo(to, collection.length)
    var nextBatch
    if (to === collection.length) {
        nextBatch = false
    } else {
        var nextFrom = computeBatchFrom(to, collection.length)
        nextBatch = {
            from: nextFrom,
            to: computeBatchTo(nextFrom + batchSize, collection.length)
        }
    }
    var prevBatch
    if (from === 0) {
        prevBatch = false
    } else {
        prevBatch = {
            from: computeBatchTo(from - batchSize, collection.length),
            to: from
        }
    }
    var firstBatch
    if (from === 0) {
        firstBatch = false
    } else {
        firstBatch = {
            from: 0,
            to: computeBatchTo(batchSize, collection.length)
        }
    }
    var lastBatch
    if (to >= collection.length) {
        lastBatch = false
    } else {
        lastBatch = {
            from: collection.length - (collection.length % batchSize),
            to: collection.length
        }
    }
    var batchedCollection = _.sortBy(collection, sortBy).slice(parseInt(from), parseInt(to))

    return {
        from: from,
        to: to,
        collectionLength: collectionLength,
        nextBatch: nextBatch,
        prevBatch: prevBatch,
        firstBatch: firstBatch,
        lastBatch: lastBatch,
        batchedCollection : batchedCollection
    }
}