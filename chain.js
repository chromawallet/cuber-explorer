module.exports = {
    getAllColoredCoins: getAllColoredCoins,
    unspentsOfAddress: unspentsOfAddress,
    getTxsForAddress: getTxsForAddress
}
var _ = require('lodash')
var query = require('./query')
var utils = require('./utils')

function getAllColoredCoins(color) {
    return query.getAllColoredCoins({color:color}).tap(_.curry(utils.log)('green', 'getAllColoredCoins')).get('data').get('coins')
}

function unspentsOfAddress(address) {
    return query.unspentsOfAddress(address).then(function(body) {
        return _.map(body.data.unspent, function(tx) {
            return tx['txid'] + ':' + tx['vount']
        })
    })
}

function getTxsForAddress(address) {
    return query.getTxsForAddress(address).get('data').get('transactions')
}

