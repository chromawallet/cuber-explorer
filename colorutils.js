// functions related to colored coins

module.exports = {
    makeColoredOutputs: makeColoredOutputs,
    isColored: isColored,
    isTransfer: isTransfer,
    isGenesis: isGenesis,
    inColors: inColors
}

var configurer = require('./config')
var config = global.config || configurer.load() //Allows for testing injection


function isColored(nSequence) {
    // Checks if nSequence indicates colored input
    // according to epobc
    return isTransfer(nSequence) || isGenesis(nSequence)
}

function inColors(output) {
    // checks if output's color property
    // is among coincolors defined in config
    if (output === null) {
        return false
    }
    for (color of config.coincolors) {
        if (output.color === color) {
            return true
        }
    }
    return false
}

function isTransfer(nSequence) {
    //Accepts a decimal nsequence number, checks if epobc's
    // transfer signature of 110011 is in it, If so, returns true
    var transferSequence = '110011'
    return isStartsequence(transferSequence, nSequence)
}

function isGenesis(nSequence) {
    //Accepts a decimal nsequence number, checks if epobc's
    // genesis signature of 100101 is in it, If so, returns true
    var genesisSequence = '100101'
    return isStartsequence(genesisSequence, nSequence)
}

function isStartsequence(sequence, nSequence) {
    var allBits = nSequence.toString(2)
    var leastSixBits = allBits.substr(allBits.length - 6)
    if (leastSixBits === sequence) {
        return true
    } else {
        return false
    }
}

function makeColoredOutputs(inputs, outputs, currentInput, currentOutput, coloredOutputs) {
    // Creates an array of outputs, where contributing inputs
    // are included in each output
    // Recursive. Initial call is made with only inputs and outputs
    // Inputs and outputs must have the property 'colorValue'
    var coloredOutputs = coloredOutputs || [] // Accumulator
    if (outputs.length === 0 && !currentOutput) {
        return coloredOutputs // Exit point
    }
    var currentInput = currentInput || getNext(inputs)
    var currentOutput = currentOutput || getNext(outputs)
    currentOutput.inputs = currentOutput.inputs || []
    currentInput.remainingValue -= currentOutput.remainingValue // deduct

    if (currentInput.remainingValue > 0) { // output is covered, input has more left
        currentOutput.inputs.push(inputInfo(currentInput)) // register input with output
        coloredOutputs.push(currentOutput) // output pushed onto accumulator
        return makeColoredOutputs(inputs, outputs, currentInput, null, coloredOutputs)

    } else if (currentInput.remainingValue < 0) { // output needs more inputs
        currentOutput.inputs.push(inputInfo(currentInput)) // register input with output
        currentOutput.remainingValue = -(currentInput.remainingValue) // output surplus
        return makeColoredOutputs(inputs, outputs, null, currentOutput, coloredOutputs)

    } else {
        currentOutput.inputs.push(inputInfo(currentInput)) // register input with output
        coloredOutputs.push(currentOutput) // output pushed onto accumulator
        return makeColoredOutputs(inputs, outputs, null, null, coloredOutputs)
    }

    function getNext(queue) {
        if (queue.length === 0) {
            return {
                remainingValue: 0
            }
        }
        var nextItem = queue.shift()
        nextItem.remainingValue = nextItem.colorValue
        return nextItem
    }

    function inputInfo(input) {
        return {
            prevTx: input.prevTx,
            outputIndex: input.outputIndex,
            nSequence: input.nSequence,
            inputIndex: input.inputIndex,
            colorValue: input.colorValue,
            isGenesis: input.isGenesis || false
        }
    }
}