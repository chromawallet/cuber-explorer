var yargs = require('yargs')
var yaml = require('js-yaml')
var fs   = require('fs');

var argv = yargs
  .usage('Usage: $0 [-h] [-c CONFIG]')
  .option('c', {
    alias: 'config',
    demand: true,
    default: 'config/default.yaml',
    describe: 'configuration file',
    type: 'string'
  })
  .help('h')
  .alias('h', 'help')
  .version(function() { return require('./package.json').version })
  .argv

function load(){
  return yaml.safeLoad(fs.readFileSync(argv.config, 'utf-8'))
}

module.exports = {
  load: load
}