module.exports = {
    getReportFor: getReportFor,
    txInfo: txInfo,
    allOutputs: allOutputs,
    coloredOutputsFromTxId: coloredOutputsFromTxId
}

var _ = require('lodash')
var P = require("bluebird");
var json2csv = P.promisify(require('json2csv'))
var utils = require('./utils')
var batcher = require('./batcher')
var input = require('./input')
var output = require('./output')
var chain = require('./chain')
var tx = require('./tx')
var configurer = require('./config')

var config = global.config || configurer.load() //Allows for testing injection

function getReportFor(colors, address) {
    return P.resolve(address)
        .then(chain.getTxsForAddress)
        .filter(tx.inBlockchain)
        .then(utils.removeDuplicates)
        .map(tx.getFullTx, {concurrency: 5})
        .map(tx.addTimestamp, {concurrency: 5})
        .map(tx.asColorTx, {concurrency: 5})
        .bind({
            colors: colors,
            address:address
        })
        .map(tx.keepOnlyColoredInputsOutputs)
        .map(output.splitIntoColoredOutputs)
        .then(_.flatten)
        .then(output.addSpentInfo)
        .map(input.addAddressesToInputs, {concurrency: 5})
        .then(makeReportData)
}

function allOutputs(colors, from, to) {
    return P.resolve(
            colors
        )
        .map(chain.getAllColoredCoins)
        .then(_.flatten)
        .filter(tx.inBlockchain)
        .filter(output.hasColorValue)
        .bind({
            from: from,
            to: to,
            batchSize: config.batchsize,
            sortBy: 'height'
        }).then(batcher.batch)
        .map(output.addOutputAddresses, {
            concurrency: 5
        }).tap(_.curry(utils.log)('blue', 'allOutputs in explorer.js'))
        .map(tx.addTimestamp, {
            concurrency: 5
        })
        .then(function(allTxos) {
            allTxos.sort(function(a, b) {
                return a.height - b.height
            })
            return {
                allTxos: allTxos,
                from: this.from,
                to: this.to,
                collectionLength: this.collectionLength,
                nextBatch: this.nextBatch,
                prevBatch: this.prevBatch,
                firstBatch: this.firstBatch,
                lastBatch: this.lastBatch
            }
        })
}

function txInfo(txId) {
    return coloredOutputsFromTxId(txId)
        .map(input.addAddressesToInputs)
}

function isNotColoredChangeTransaction(colorTx) {
    // If all input addresses are equal to the output address
    // then this is purely a circular tx for giving back change
    var inputAddresses = colorTx.inputs.map(function(thisInput) {
        return thisInput.fromAddress
    })
    return _.difference(inputAddresses, [colorTx.address]).length > 0
}

function spentFromAddress(colorTx, accountAddress) {
    // If at least one input is the same as the account address
    // then this colored transaction gets listed
    // as coming from the account address
    var inputAddresses = colorTx.inputs.map(function(thisInput) {
        return thisInput.fromAddress
    })
    if (_.include(inputAddresses, accountAddress)) {
        utils.log('yellow', 'spentFromAddress ' + accountAddress  + ' is in', inputAddresses)
    }
    return _.include(inputAddresses, accountAddress)
}

function asEuro(colorValue) {
    return parseFloat((colorValue / 100.0).toFixed(2))
}

function markSpents(spent) {
    spent.isSpent = true // for template conditionals
    spent.euro = -spent.euro
    return spent
}

function asCSVHistory(transaction) {
    return {
        address: transaction.address,
        amount: transaction.euro,
        date: transaction.isoTimestamp
    }
}

function makeReportData(coloredOutputs) {
    var address = this.address
    utils.log('cyan', 'input to makeReportdata', [coloredOutputs, address])
    var report = {}
    report.address = address
    coloredOutputs = coloredOutputs.map(function(thisOutput) {
        thisOutput.euro = asEuro(thisOutput.colorValue)
        return thisOutput
    })
    utils.log('red',"Unspents actually coloredOutputs",coloredOutputs)

    var unspentsToAddress = _.filter(coloredOutputs, {
        unspent: true,
        address: address
    })
    if (unspentsToAddress.length === 0) {
        report.balance = 0
    } else if (unspentsToAddress.length === 1) {
        report.balance = asEuro(unspentsToAddress[0].colorValue)
    } else {
        report.balance = asEuro(unspentsToAddress.reduce(function(sumSoFar, currentObject) {
            return sumSoFar + currentObject.colorValue
        },0))
    }
    report.spents = coloredOutputs.filter(function(thisOutput) {
        return spentFromAddress(thisOutput, address)
    }).filter(isNotColoredChangeTransaction).map(markSpents)
    report.incoming = _.filter(coloredOutputs, {
        address: address
    }).filter(isNotColoredChangeTransaction)
    var tx_history = report.incoming.concat(report.spents)
    report.tx_history = _.sortByOrder(tx_history, ['isoTimestamp', 'euro'], ['desc', 'asc'])
    report.tx_history_for_csv = json2csv({
        fields: ['address', 'amount', 'date'],
        data: report.tx_history.map(asCSVHistory)
    })
    return report
}


function coloredOutputsFromTxId(txId) {
    return tx.getTxInfo(txId).then(function(thisTx) {
            return tx.asColorTx(thisTx)
        })
        .then(function(thisTx) {
            return tx.keepOnlyColoredInputsOutputs.call({
                colors: config.coincolors
            }, thisTx)
        })
        .then(output.splitIntoColoredOutputs)
}