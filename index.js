var express = require('express');
var bodyParser = require('body-parser');
var explorer = require('./explorer')
var configurer = require('./config')

var config = global.config || configurer.load() //Allows for testing injection
var euroColors = config.coincolors
var app = express();

app.set('view engine', 'jade');
app.use('/res', express.static('views/static'));
app.use(bodyParser.urlencoded({
    extended: false
}))

app.get('/', function(req, res) {
    res.render('index')
})

app.get('/getreportfor', function(req, res) {
    res.render('getoutputs')
})

app.get('/getreportfor/:address', function(req, res) {
    var address = req.params.address
    handleGetReport(req, res, address)
})

app.get('/getcsvreportfor/:address', function(req, res) {
    var address = req.params.address
    explorer.getReportFor(euroColors, address).get('tx_history_for_csv').done(
        function(data) {
            res.setHeader('Content-disposition', 'attachment; filename=' + address + '.csv')
            res.setHeader('Content-type', 'text/csv')
            res.send(data)
        }, function(err) {
            console.log(err.stack || err)
            res.status(500).send(err.toString());
        }

    )
})

app.post('/getreportfor', function(req, res) {
    var address = req.body.address
    handleGetReport(req, res, address)
})

app.get('/blockchain', function(req, res) {
    var from = parseInt(req.query.from) || 0
    var to = parseInt(req.query.to) || parseInt(from) + config.batchsize
    // Prevent DOS:ing of call by too big a batch
    if (parseInt(to) > (from + config.batchsize)) {
        to = parseInt(from) + config.batchsize
    }
    explorer.allOutputs(euroColors, from, to)
        .done(
            function(data) {
                res.render('blockchain', {
                    data: data || [],
                })
            }, function(err) {
                console.log(err.stack || err)
                res.status(500).send(err.toString());
            })
})

app.get('/txinfo/:txid', function(req, res) {
    txId = req.params.txid
    explorer.txInfo(txId).done(
        function(data) {
            res.render('txinfo', {
                tx: data,
                path: req.path
            })
        }, function(err) {
            console.log(err.stack || err)
            res.status(500).send(err.toString());
        })
})

function handleGetReport(req, res, address) {
    explorer.getReportFor(euroColors, address)
        .done(
            function(data) {
                res.render('outputsinfo', {
                    report: data
                })
            }, function(err) {
                console.log(err.stack || err)
                res.status(500).send(err.toString());
            })
}

var server = app.listen(config.port, function() {
    console.log('listening on ' + config.port);
});