module.exports = {
    addAddressesToInput: addAddressesToInput,
    addAddressesToInputs: addAddressesToInputs,
    decorateInputWithColorInfo: decorateInputWithColorInfo,
    getAddress: getAddress,
    getAddressOfPrevious: getAddressOfPrevious,
    isColored: isColored
}
var _ = require('lodash')
var P = require("bluebird")
var output = require('./output')
var tx = require('./tx')
var explorer = require('./explorer')
var utils = require('./utils')
var colorutils = require('./colorutils')
var configurer = require('./config')

var config = global.config || configurer.load() //Allows for testing injection

function addAddressesToInputs(obj) {
    // Adds colorValue info to the inputs of
    // transaction tx
    return utils.mapProperty(obj, 'inputs', addAddressesToInput)
}

function addAddressOfPrevious(thisInput) {
    return getAddressOfPrevious(thisInput.prevTx, thisInput.outputIndex).then(
        function(address) {
            thisInput.fromAddress = address
            return thisInput
        }
    )
}

function getAddressOfPrevious(prevTx, outputIndex) {
    return P.resolve(prevTx).then(tx.getTxInfo).then(function(thisTx) {
        return getAddress(thisTx, outputIndex)
    })
}

function addAddressesToInput(thisInput) {
    return addAddressOfPrevious(thisInput)
}

function decorateInputWithColorInfo(thisInput, colorValues) {
    if (colorValues === null) {
        thisInput.ColorValue = 0
        thisInput.color = null
    } else {
        utils.log('yellow', 'in decorateInputWithColorInfo', [thisInput, colorValues])
        try {
            thisInput.colorValue = colorValues[thisInput.outputIndex] ? colorValues[thisInput.outputIndex].value : 0
        } catch (err) {
            debugger
        }
        thisInput.color = colorValues[thisInput.outputIndex] ? colorValues[thisInput.outputIndex].color : null
    }
    return thisInput
}

function isColored(input) {
    return colorutils.isColored(input.sequenceNumber)
}

function getAddressOfReferencedOutput(thisInput) {
    if (thisInput.isGenesis) {
        return getAddressOfPrevious(thisInput.prevTx, thisInput.outputIndex)
    } else {
        return tx.getTxInfo(thisInput.prevTx).then(function(thisTx) {
            return getAddress(thisTx, thisInput.outputIndex)
        })
    }
}

function getAddress(thisTx, index) {
    var address = thisTx.outputs[index].script.toAddress(config.addressformat).toString()
    return address
}