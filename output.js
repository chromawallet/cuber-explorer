module.exports = {
    addOutputAddresses: addOutputAddresses,
    addSpentInfo: addSpentInfo,
    annotateUnspent: annotateUnspent,
    splitIntoColoredOutputs: splitIntoColoredOutputs,
    hasColorValue: hasColorValue
}

var _ = require('lodash')
var utils = require('./utils')
var chain = require('./chain')
var colorutils = require('./colorutils')
var tx = require('./tx')
var configurer = require('./config')
var config = global.config || configurer.load() //Allows for testing injection


function addOutputAddresses(txInfo) {
    utils.log('red', 'txInfo in addOutputAddresses', txInfo)
    return tx.getTxInfo(txInfo.txId)
        .then(function(thisTx) {
            utils.log('yellow', 'thisTx in addOutputAddresses', thisTx)
            txInfo.address = thisTx.outputs[txInfo.outIndex].script.toAddress(config.addressformat).toString()
            txInfo.euro = (txInfo.colorValue / 100.0).toFixed(2)
            txInfo.outputId = txInfo.txId + ':' + txInfo.outIndex
            return txInfo
        })
}

function addSpentInfo(coloredOutputs) {
    return chain.unspentsOfAddress(this.address).then(function(unspents) {
        return annotateUnspent(coloredOutputs, unspents)
    })
}

function annotateUnspent(coloredOutputs, unspents) {
    utils.log('yellow', 'annotateUnspent, unspents are:', unspents)
    utils.log('yellow', 'annotateUnspent, coloredOutputs outputids are:', coloredOutputs.map(function(foo) {
        return foo.outputId
    }))
    return coloredOutputs.map(function(coloredOutput) {
        if (unspents.indexOf(coloredOutput.outputId) > -1) {
            coloredOutput.unspent = true
        } else {
            coloredOutput.unspent = false
        }
        return coloredOutput
    })
}

function splitIntoColoredOutputs(coloredTx) {
    if (coloredTx.inputs.length === 0) { // Genesis transaction
        return coloredTx.outputs.map(function(thisOutput) {
            thisOutput.inputs = []
            thisOutput.isGenesis = true
            thisOutput.txId = coloredTx.txId
            return thisOutput
        })

    } else {
        utils.log('yellow', 'inputs to makeColoredOutputs', coloredTx.inputs)
        utils.log('yellow', 'outputs to makeColoredOutputs', coloredTx.outputs)

        return colorutils.makeColoredOutputs(coloredTx.inputs, coloredTx.outputs).map(function(thisOutput) {
            thisOutput.txId = coloredTx.txId
            thisOutput.blockchainTx = coloredTx.blockchainTx
            return thisOutput
        })
    }
}

function hasColorValue(thisOutput) {
    return thisOutput.colorValue
}