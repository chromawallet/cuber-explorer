module.exports = {
    addTimestamp: addTimestamp,
    callGetTxColorValues: callGetTxColorValues,
    getAllColoredCoins: getAllColoredCoins,
    getFullTx: getFullTx,
    getTxsForAddress: getTxsForAddress,
    unspentsOfAddress: unspentsOfAddress
}


var prequest = require("prequest")
var _ = require('lodash')
var configurer = require('./config')
var utils = require('./utils')
var colorutils = require('./colorutils')


var config = global.config || configurer.load() //Allows for testing injection
var maxBlockheight = config.maxblockheight || false // used for testing

function constrainBlockheight(queryString) {
    // Used for testing, so that the outcome for every address is known and immutable
    // if maxblockheight is falsy (including undefined), do 
    // not constrain blockheight
    if (!maxBlockheight) {
        return queryString
    }
    return queryString + '&to=' + maxBlockheight
}


function getFullTx(txId) {
    var url = config.chromanodeUrl + '/v1/transactions/raw?txid=' + txId
    return GETData(url)
}


function addTimestamp(txInfo) {
    var url = config.chromanodeUrl + '/v1/headers/query?from=' + txInfo.height + '&count=1'
    utils.log('magenta', 'In addTimestamp', url)
    return GET(url)
}


function unspentsOfAddress(address) {
    var url = constrainBlockheight(config.chromanodeUrl + '/v2/addresses/query?status=unspent&addresses=' + address)
    return GET(url)

}


function getTxsForAddress(address) {
    var url = constrainBlockheight(config.chromanodeUrl + '/v1/addresses/query?addresses=' + address)
    return GET(url)
}


function callGetTxColorValues(requestBody) {
    uri = config.scannerUrl + 'getTxColorValues'
    requestBody.inputs = true
    return POSTJSON(uri, requestBody).tap(_.curry(utils.log)('red', 'callGetTxColorValues')).get('data')
}


function getAllColoredCoins(requestBody) {
    var uri = config.scannerUrl + 'getAllColoredCoins'
    return POST(uri, requestBody).tap(_.curry(utils.log)('yellow', 'getAllColoredCoins in query.js returns'))
}


function POST(uri, requestBody) {
    return doQuery({
        method: 'POST',
        url: uri,
        body: requestBody,
        json: true
    })
}


function POSTJSON(uri, json) {
    var params = {
        method: 'POST',
        url: uri,
        json: json,
    }
    return doQuery(params)
}

function GET(url) {
    var params = {
        method: 'GET',
        url: url,
        json: true
    }
    return doQuery(params)
}

function GETData(url) {
    var params = {
        method: 'GET',
        url: url,
        json: true
    }
    return prequest(params).get('data').get('hex')
}

function doQuery(params) {
    return prequest(params)
        .then(function(body) {
            return body
        })
}