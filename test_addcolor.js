var input = require('./input')
var utils = require('./utils')
var _ = require('lodash')


var genesisInput = {
      "prevTx": "f1e36e6692a3dd3b8074ebc13c7d2a189907f87884951a5a4a92f18013ad2bbc",
      "outputIndex": 0,
      "nSequence": 37,
      "inputIndex": 0
    }

var stepAfterGenesisInput = {
      "prevTx": "4ed18f46bc7becddc57d91cb4bb825f0cbb8e3bef7ef0bd1be1c9fb786c67ea6",
      "outputIndex": 0,
      "nSequence": 691,
      "inputIndex": 0
    }

// input.addAddressesToInput(genesisInput).done(_.curry(utils.log)('green', 'Result'))
input.addAddressesToInput(stepAfterGenesisInput).done(_.curry(utils.log)('green', 'Result'))

// mqoFE7GD3F3WqBqfrXofqshbdcGMBWNGSx is where the funding of the genesis transaction comes from
// It is the answer we want for the the genesis input, and it is the answer that we do get
// 92z3y92UyCBWL6RQuR36eYFLPdjjyJwa23NXuaGxmNDZJRv5n9Q

// mxcJHJ5hMd8pZxUYwqdkyCqyoouRFzeHWn is the address that can divy up the parts of the asset
// It is the answer we want for the stepAfterGenesisInput, but instead we get the mqoFE7GD3F3WqBqfrXofqshbdcGMBWNGSx
// again

// moAbsEpcdAJYwfX39QwnwoT9gnMBLU4K99 is the address that got the two lots of assets

// New asset the bar_inc asset epobc:a05288711f0bf3c50984fac543a6ebb6fadd972f6f16894f9fafa60dee02f512:0:2311
// Issued in 10 units and with a longer chains of transfers between addresses such as:
// [u'7gp5mcDr4eQYo7@mgQyDS7hNquPvUF5sEFfZ9ccsB8ASynfY9',
 // u'7gp5mcDr4eQYo7@mvv4sU3a8yiWWTrnwqZbwUuNbL4ChXEfw9']
 // [u'7gp5mcDr4eQYo7@n3WFzivZKmftButGa47x2i9LwjgvC5NHQe',
 // u'7gp5mcDr4eQYo7@mq7hsbBGV3iH4f4sgCKfTU6tAL5YwkJ1SV']

 // Transaction history, as it happened for asset bar_inc
 // Created with this asset info:
 // {u'assetid': u'7gp5mcDr4eQYo7',
 // u'color_set': [u'epobc:a05288711f0bf3c50984fac543a6ebb6fadd972f6f16894f9fafa60dee02f512:0:2311'],
 // u'monikers': [u'bar_inc'],
 // u'unit': 1}
 // Address n3WFzivZKmftButGa47x2i9LwjgvC5NHQe gets 4 items, from genesis transaction
 // Address mgQyDS7hNquPvUF5sEFfZ9ccsB8ASynfY9 gets 2 items, from n3WFzivZKmftButGa47x2i9LwjgvC5NHQe
 