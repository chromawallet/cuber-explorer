var vows = require("vows")
var assert = require('assert')

var P = require('bluebird')
var colorutils = require('../colorutils')
var batcher = require('../batcher')

vows.describe('Array').addBatch({ // Batch
    'Test makeColoredOutputs (matches outputs with inputs according to their color values)': { // Context
        '3 inputs, 3 outputs overlapping, should check balance and check out': { // Sub-Context
            topic: function() {
                var inputs = [{
                    prevTx: 1,
                    colorValue: 2
                }, {
                    prevTx: 2,
                    colorValue: 4
                }, {
                    prevTx: 3,
                    colorValue: 2
                }]
                var outputs = [{
                    name: 1,
                    colorValue: 1
                }, {
                    name: 2,
                    colorValue: 6
                }, {
                    name: 3,
                    colorValue: 1
                }]

                return (colorutils.makeColoredOutputs(inputs, outputs, null, null, null))
            }, // Topic

            'Correct number of inputs per output': function(topic) { // Vow
                assert.equal(topic[0].inputs.length, 1)
                assert.equal(topic[1].inputs.length, 3)
                assert.equal(topic[0].inputs.length, 1)

            }
        },

        '2 inputs, 1 output  should check balance and check out': { // Sub-Context
            topic: function() {
                var inputs = [{
                    "prevTx": "a1b894ae070f780504211d2fbcb0a54fe1a5d43d293eda266e64b44502e47e19",
                    "outputIndex": 0,
                    "nSequence": 691,
                    "inputIndex": 0,
                    "colorValue": 300,
                    "color": "epobc:a254bd1a4f30d3319b8421ddeb2c2fd17893f7c6d704115f2cb0f5f37ad839af:0:0"
                }, {
                    "prevTx": "0528e70aa56dc4d70c7e794dd553a2440d03658209f693c786453ad53271113f",
                    "outputIndex": 1,
                    "nSequence": 4294967295,
                    "inputIndex": 1,
                    "colorValue": 64,
                    "color": "epobc:a254bd1a4f30d3319b8421ddeb2c2fd17893f7c6d704115f2cb0f5f37ad839af:0:0"
                }]
                var outputs = [{
                    "address": "1ETj6AxNH6WgWCzGoVNDGJtX617NEChgAS",
                    "colorValue": 364,
                    "outputId": "2f6f45c76a99a39ac6e0595cdbe0a98dfffb57b5d824eeed0b39b8438549223d:0",
                    "isoTimestamp": "2015-08-04T20:27:02.000Z",
                    "timestamp": 1438720022
                }]

                return (colorutils.makeColoredOutputs(inputs, outputs, null, null, null))
            }, // Topic

            'Correct number of inputs per output': function(topic) { // Vow
                assert.equal(topic[0].inputs.length, 2)

            },


        },

    }
}).run()

vows.describe('Batching').addBatch({ // Batch
    'From 0 and to 2 should yield': { // Sub-Context
        topic: function() {
            var from = 0
            var to = 2
            var batchSize = 2
            var sortBy = 'height'
            var collection = [{
                height: 1
            }, {
                height: 2
            }, {
                height: 3
            }, {
                height: 4
            }, {
                height: 5
            }, {
                height: 6
            }]
            return batcher.doBatchWork(collection, from, to, batchSize, sortBy)
        }, // Topic
        '0 and 2 back': function(topic) {
            assert.equal(topic.from, 0)
            assert.equal(topic.to, 2)
        },
        'first batch should not exist': function(topic) {
            assert.equal(topic.firstBatch, false)
        },
        'but last batch should exist': function(topic) {
            assert(topic.lastBatch)
        }


    },
    'From 2 and to 4 should yield': { // Sub-Context
        topic: function() {
            var from = 2
            var to = 4
            var batchSize = 2
            var sortBy = 'height'
            var collection = [{
                height: 1
            }, {
                height: 2
            }, {
                height: 3
            }, {
                height: 4
            }, {
                height: 5
            }, {
                height: 6
            }]
            return batcher.doBatchWork(collection, from, to, batchSize, sortBy)
        }, // Topic
        '2 and 4 back': function(topic) {
            assert.equal(topic.from, 2)
            assert.equal(topic.to, 4)
        },
        'Previous batch should exist': function(topic) {
            assert(topic.prevBatch)
        },
        'Next batch should exist': function(topic) {
            assert(topic.nextBatch)
        },
        'First batch should exist': function(topic) {
            assert(topic.firstBatch)
        },
        'And last batch should exist': function(topic) {
            assert(topic.lastBatch)
        }
    },
    'From 20 to 40 should yield': { // Sub-Context
        topic: function() {
            var from = 20
            var to = 40
            var batchSize = 2
            var sortBy = 'height'
            var collection = [{
                height: 1
            }, {
                height: 2
            }, {
                height: 3
            }, {
                height: 4
            }, {
                height: 5
            }, {
                height: 6
            }]
            return batcher.doBatchWork(collection, from, to, batchSize, sortBy)
        }, // Topic
        '5 and 6 back': function(topic) {
            assert.equal(topic.from, 5)
            assert.equal(topic.to, 6)
        },
        'First should exist': function(topic) {
            assert(topic.firstBatch)
        },
        'but not last': function(topic) {
            assert.equal(topic.lastBatch, false)
        }

    },

    'From 2 to 40 should yield': { // Sub-Context
        topic: function() {
            var from = 2
            var to = 40
            var batchSize = 2
            var sortBy = 'height'
            var collection = [{
                height: 1
            }, {
                height: 2
            }, {
                height: 3
            }, {
                height: 4
            }, {
                height: 5
            }, {
                height: 6
            }]
            return batcher.doBatchWork(collection, from, to, batchSize, sortBy)
        }, // Topic
        '2 and 6 back': function(topic) {
            assert.equal(topic.from, 2)
            assert.equal(topic.to, 6)

        }

    },
}).run()

// vows.describe('Get addresses of inputs').addBatch({ // Batch
//     'Test addAddressesToInput (Chases down the addresses of the oputputs pointing with cor values to the inputs in the transaction whose utput points at our transaction)': { // Context
//         'See if we get an array of addresses back': { // Sub-Context
//             topic: function() {
//                 var input = require('../input')

//                 return input.addAddressesToInput({
//                     prevTx: '4ed18f46bc7becddc57d91cb4bb825f0cbb8e3bef7ef0bd1be1c9fb786c67ea6',
//                     outputIndex: 0,
//                     nSequence: 691,
//                     inputIndex: 0,
//                     colorValue: 1000,
//                     color: 'epobc:4ed18f46bc7becddc57d91cb4bb825f0cbb8e3bef7ef0bd1be1c9fb786c67ea6:0:0'
//                 }).done()
//             }, // Topic

//             'Contains an array of strings': function(topic) { // Vow
//                 assert.true(Array.isArray(topic))
//             },
//             'Matches exactly the desired output': function(topic) {
//                 assert.equal(topic, {
//                     prevTx: '4ed18f46bc7becddc57d91cb4bb825f0cbb8e3bef7ef0bd1be1c9fb786c67ea6',
//                     outputIndex: 0,
//                     nSequence: 691,
//                     inputIndex: 0,
//                     colorValue: 1000,
//                     color: 'epobc:4ed18f46bc7becddc57d91cb4bb825f0cbb8e3bef7ef0bd1be1c9fb786c67ea6:0:0',
//                     fromAddresses: ['mqoFE7GD3F3WqBqfrXofqshbdcGMBWNGSx']
//                 })
//             }
//         },

//     }
// }).run()