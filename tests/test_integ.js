// tests
// require('longjohn')
var _ = require('lodash')

GLOBAL.config = {
    chromanodeUrl: "http://v1.livenet.bitcoin.chromanode.net",
    scannerUrl: "http://v1.livenet.bitcoin.chromanode.net/v2/cc/",
    coincolors: [
        'epobc:a254bd1a4f30d3319b8421ddeb2c2fd17893f7c6d704115f2cb0f5f37ad839af:0:0',
        'epobc:c053bbd82615c057a5835584a739c16c0e5ccd4823150c1a5144461b6899659d:0:0'
    ],
    addressformat: 'mainnet',
    batchsize: 50,
    // maxblockheight: 385909
    // maxblockheight: 386040
    // maxblockheight: 386360,
    debug: true,
    debugPattern: 'spentFromAddress'


}

var explorer = require('../explorer')
var chain = require('../chain')


function compareValues(address, report) {
    console.log("Testing address " + address)
    var balance = report.balance.toFixed(2)

    var computedBalance = report.tx_history.reduce(function(sumSoFar, currentObject) {
        return sumSoFar + Number(currentObject.euro)
    }, 0)
    if (balance === computedBalance.toFixed(2) || (balance === '0.00' && computedBalance.toFixed(2) === '-0.00')) {
        // console.log(address + "pass")
        // console.log("Balance is " + balance)
        // console.log("Computed balance is " + computedBalance.toFixed(2))
        // console.log("endpass")
    } else {
        console.log("Fail: " + address + ". Balance is " + balance + ". Computed balance is " + computedBalance.toFixed(2))
    }

}


function runTest(address, allowLarge) {
    console.log("Testing number of txs of address " + address + '...')
    var maxTx = 20
    return chain.getTxsForAddress(address).then(function(txs) {
        if (txs.length < maxTx || allowLarge) {
            console.log('Address ' + address + ' has less than ' + maxTx + ' txs, will be used...')
            return explorer.getReportFor(config.coincolors, address).done(_.curry(compareValues)(address))
        } else {
            return null
        }
    }).done()



}

// Get a couple of addresses, we would like to check
explorer.allOutputs(config.coincolors, 8000, 8050).get('allTxos').map(function(obj) {
    return obj.address
}).then(_.uniq).map(runTest, {
    concurrency: 1
}).done()

runTest('1ETj6AxNH6WgWCzGoVNDGJtX617NEChgAS', true)