module.exports = {
    addTimestamp: addTimestamp,
    asColorTx: asColorTx,
    callGetTxColorValues: callGetTxColorValues,
    getColorValues: getColorValues,
    getFullTx: getFullTx,
    getTxInfo: getTxInfo,
    isColored: isColored,
    keepOnlyColoredInputsOutputs: keepOnlyColoredInputsOutputs,
    inBlockchain: inBlockchain

}
var _ = require('lodash')
var bitcore = require('bitcore')
var query = require('./query')
var output = require('./output')
var input = require('./input')
var colorutils = require('./colorutils')
var utils = require('./utils')

function asColorTx(thisTx) {
    // Takes a bitcore-format transaction,
    // returns a colored transaction as pure JSON
    return getColorValues(thisTx).tap(_.curry(utils.log)('magenta', 'tx.asColorTx after getColorValues')).then(function(colorValuesData) {
        // debugger
        var colorValues = colorValues
        var colorTx = {}

        colorTx.outputs = _.map(colorValuesData.colorValues, function(item, i) {
            return {
                address: input.getAddress(thisTx, i),
                colorValue: item ? item.value : 0,
                outputId: thisTx.hash + ':' + i,
                isoTimestamp: thisTx.isoTimestamp,
                timestamp: thisTx.timestamp,
                blockchainTx: thisTx.toJSON(),
                color: item ? item.color : null
            }
        })
        colorTx.inputs = _.map(thisTx.inputs, function(input, i) {
            var coloredInput = {}
            coloredInput.prevTx = input.prevTxId.hexSlice()
            coloredInput.outputIndex = input.outputIndex
            coloredInput.nSequence = input.sequenceNumber
            coloredInput.inputIndex = i
            if (colorValuesData.inputColorValues[i] != null) {
                coloredInput.color = colorValuesData.inputColorValues[i].color
                coloredInput.colorValue = colorValuesData.inputColorValues[i].value
            } else {
                coloredInput.color = null
                coloredInput.colorValue = 0

            }
            return coloredInput

        })


        colorTx.height = thisTx.height
        colorTx.txId = thisTx.hash
        colorTx.isoTimestamp = thisTx.isoTimestamp
        return colorTx
    })

}

function getColorValues(thisTx) {
    // Returns all color values for transaction
    return callGetTxColorValues(thisTx.hash, _.range(thisTx.outputs.length))
}

function addTimestamp(txInfo) {
    return query.addTimestamp(txInfo).tap(_.curry(utils.log)('cyan', 'addTimestamp in tx.js')).then(function(body) {
        var header = new bitcore.BlockHeader.fromString(body.data.headers)
        txInfo.timestamp = header.timestamp
        var date = new Date(parseInt(header.timestamp) * 1000)
        txInfo.isoTimestamp = date.toISOString();
        return txInfo
    })
}

function callGetTxColorValues(hash, outIndices) {
    requestBody = {
        txId: hash
    }
    var result = query.callGetTxColorValues(requestBody)
    return result
}

function getFullTx(thisTx) {
    return getTxInfo(thisTx.txid).then(function(txInfo) {
        txInfo.height = thisTx.height
        return txInfo
    })
}

function getTxInfo(txId) {
    return query.getFullTx(txId).then(function(rawTx) {
        txInfo = new bitcore.Transaction(rawTx)
        return txInfo
    })
}

function keepOnlyColoredInputsOutputs(coloredTx) {
    // Keeps colored inputs, outputs and the genesis input
    // debugger
    var colors = this.colors
        // inputs may be out of order due to previous async processing
    coloredTx.inputs.sort(function(a, b) {
        return a.inputIndex - b.inputIndex
    })
    // Remove null inputs
    var inputs = coloredTx.inputs.filter(function(thisInput) {
            return thisInput
        })
        // Keep input if it has a genesis nSequence marker
    genesisInputs = inputs.filter(function(thisInput) {
        return colorutils.isGenesis(thisInput.nSequence)
    })
    // Keep inputs of the right colors
    inputs = genesisInputs.concat(inputs.filter(function(thisInput) {
        return colors.indexOf(thisInput.color) > -1
    }))
    coloredTx.inputs = inputs
    coloredTx.outputs = coloredTx.outputs.filter(function(thisOutput) {
        return colors.indexOf(thisOutput.color) > -1
    })
    return coloredTx
}

function isColored(thisTx) {
    return _.some(thisTx.inputs, input.isColored)
}

function inBlockchain(thisTx) {
    return thisTx.height
}