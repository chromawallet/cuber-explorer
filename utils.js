module.exports = {
    mapProperty: mapProperty,
    removeDuplicates: removeDuplicates,
    log: log
}

var P = require("bluebird");
var _ = require('lodash')
var colors = require('colors/safe')
var configurer = require('./config')

var config = global.config || configurer.load() //Allows for testing injection

if (config.debugPattern) {
    var debugPattern = new RegExp(config.debugPattern)
} else {
    var debugPattern = /.*/
}

function removeDuplicates(arrayOfObjects) {
    return _.filter(arrayOfObjects, function(element, index) {
        for (index += 1; index < arrayOfObjects.length; index += 1) {
            if (_.isEqual(element, arrayOfObjects[index])) {
                return false;
            }
        }
        return true;
    });
}

function mapProperty(obj, propertyName, processFunction) {
    // Takes an object, processes a map-friendly property on
    // it, with "map" and processFunction. Then returns a
    // promise of the object with an array of processFunction's 
    // return values in the property.

    return P.resolve(obj).then(
        function(obj) {
            return (P.all(
                obj[propertyName].map(
                    processFunction
                )
            ).then(
                function(result) {
                    obj[propertyName] = result;
                    return obj
                }
            ))
        }
    )
}

function log(logColor, message, data) {
    if (config.debug && message.match(debugPattern)) {
        if (data) {
            console.log(message)
            console.log(colors[logColor](JSON.stringify(data, null, 2)))
        }
    }
}