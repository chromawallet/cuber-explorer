var tzShortFormatter = new Intl.DateTimeFormat(["en"], {
    hour: 'numeric',
    minute: 'numeric',
    timeZoneName: "short",
})
var tzLongFormatter = new Intl.DateTimeFormat(["en"], {
    day: 'numeric',
    year: 'numeric',
    month: 'short',
    timeZoneName: "long",
})

var timestampFormatter = new Intl.DateTimeFormat(["en"], {
    hour: 'numeric',
    minute: 'numeric',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    timeZoneName: "short"
})

function display_in_tz() {
    var date = new Date($(this).text())
    $(this).text(timestampFormatter.format(date))
}

function updateTzInfo() {
    var date = new Date()
    var tzInfoShortFormat = tzShortFormatter.format(date)
    var tzInfoLongFormat = tzLongFormatter.format(date)
    $('#current_tz').text(tzInfoShortFormat + ' (' + tzInfoLongFormat + ')')
    setTimeout(updateTzInfo, 4000)
}

$(document).ready(function() {
    updateTzInfo()
    $('.parsable_timestamp').each(display_in_tz)
    $('#tz-help').hide()
    $('#tz-help').click(function() {
        $('#tz-help').hide();
    });
    $('#show-tz-help').click(function() {
        $('#tz-help').show();
    });
})